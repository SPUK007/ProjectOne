package stepDefinitions;

import io.cucumber.java.*;
import io.cucumber.java.en.*;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class StepDefinitions {
    private WebDriver webDriver;
    private String baseUrl = "https://www.amazon.co.uk";

    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver","C:/Users/WinUser/IdeaProjects/WebDrivers/chromedriver.exe");
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
    }

    @Given("Open amazon.com")
    public void open_amazon_com() {
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        webDriver.get(baseUrl);
    }

    @When("Entering search iPhone")
    public void entering_search_iPhone() {
        WebElement googleTextBox = webDriver.findElement(By.xpath("//*[@id='twotabsearchtextbox']"));
        googleTextBox.sendKeys("Iphone X 256");
    }

    @Then("Result should be")
    public void result_should_be() {
        WebElement searchButton = webDriver.findElement(By.xpath("//*[@id=\'nav-search-submit-text\']/input"));
        searchButton.click();
        WebElement searchResult = webDriver.findElement(By.id("search"));
        System.out.println("Search Result : "+ searchResult);
        webDriver.close();
    }
    @After
    public void end() {
        if (webDriver != null) {
            webDriver.quit();
        }
    }

}

